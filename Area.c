//Calculate area of different shapes

#include <stdio.h>

int main(){
	float a, b;
	int shape;
	
	printf("Area of:\n1. Circle\n2. Square\n3. Rectangle\n4. Triangle\n\n");
	scanf("%d", &shape);
	printf("\n");
	
	if(shape == 1){
		printf("Enter the radius: ");
		scanf("%f", &a);
		printf("\n");
		
		a = 3.1415 * a * a;
	}
	else if(shape == 2){
		printf("Enter the side length: ");
		scanf("%f", &a);
		printf("\n");
		
		a = a * a;
	}
	else if(shape == 3){
		printf("Enter the side lengths: ");
		scanf("%f%f", &a, &b);
		printf("\n");
		
		a = a * b;
	}
	else if(shape == 4){
		printf("Enter the base and height: ");
		scanf("%f%f", &a, &b);
		printf("\n");
		
		a = 0.5 * a * b;
	}
	
	if(a - ((int) a) == 0)
		printf("Area is: %.0f", a);
	else
		printf("Area is: %.3f", a);
	
	return 0;
}