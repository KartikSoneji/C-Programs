//Perform basic Arithmetic Operations using Pointers

#include <stdio.h>
#include <stdlib.h>

int main(){
	int *a, *b;
	a = malloc(sizeof(int));
	b = malloc(sizeof(int));
	
	printf("Enter two numbers:\n");
	scanf("%d%d", a, b);
	printf("\n");
	
	printf("a + b = %d\n", *a + *b);
	printf("a - b = %d\n", *a - *b);
	printf("a * b = %d\n", *a * *b);
	if(*b != 0)
		printf("a / b = %d\n", *a / *b);
	printf("a % b = %d\n", *a % *b);
	
	free(a);
	free(b);
	
	return 0;
}