//Check whether a number is Even or Odd

#include <stdio.h>

int main(){
	int a;
	
	printf("Enter a number: ");
	scanf("%d", &a);
	printf("\n");
	
	printf("Number is %s", (a % 2 == 0)?"Even":"Odd");
	
	return 0;
}