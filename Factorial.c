//Find the Factorial of a number

#include <stdio.h>

int main(){
	unsigned int a;
	unsigned long long int f;
	
	printf("Enter a number: ");
	scanf("%u", &a);
	printf("\n");
	
	f = a;
	while(a-- > 1)
		f *= a;
	
	printf("Factorial is %llu", f);
	
	return 0;
}