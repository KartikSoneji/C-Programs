//Print the Fibonacci series

#include <stdio.h>

int main(){
	int n;
	
	printf("Enter the number of terms: ");
	scanf("%d", &n);
	printf("\n");
	
	for(int i = 0, a = 0, b = 1, c; i < n; i++){
		c = a + b;
		b = a;
		a = c;
		
		printf("%d ", c);
	}
	printf("\n");
	
	return 0;
}