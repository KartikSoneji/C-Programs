//Linear Search

#include <stdio.h>
#include <stdlib.h>

int main(){
	int* a, length, inputLength, n;
	char input[1024];
	
	printf("Enter the array, separated by space: ");
	gets(input);
	
	printf("Enter a number: ");
	scanf("%d", &n);
	printf("\n");
	
	inputLength = 0;
	while(input[inputLength] != '\0')
		inputLength++;
	while(input[inputLength - 1] == ' ')
		inputLength--;
	
	length = 0;
	for(int i = 0; i < inputLength - 1; i++)
		if((i == 0) || (input[i] == ' ' && input[i + 1] != ' '))
			length++;
	
	a = malloc(length * sizeof(int));
	for(int i = 0; i < length; i++)
		a[i] = 0;
	
	for(int i = 0, x = -1, isNegative = 0; i < inputLength; i++){
		if((i == 0) || (input[i] == ' ' && input[i + 1] != ' '))
			x++;
		
		if(input[i] == '-')
			isNegative = 1;
		
		if(input[i] == ' ' || input[i] == '-')
			continue;
		
		a[x] = a[x] * 10 + (isNegative?(-1):(1)) * (input[i] - '0');
	}
	
	int index = -1;
	for(int i = 0; i < length; i++)
		if(a[i] == n){
			index = i;
			break;
		}
	
	if(index == -1)
		printf("%d not found in array.", n);
	else	
		printf("%d found at index %d.", n, index);
	printf("\n");
	
	return 0;
}
