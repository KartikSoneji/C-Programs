//Add two Matrices

#include <stdio.h>
#include <stdlib.h>

void getMatrix(int[], int*, int*);
void getArray(int[], int*);
void printMatrix(int[], int, int);

void addMatrices(int[], int[], int[]);

int main(){
	int a[64 * 64], r, c;
	int b[64 * 64];
	int sum[64 * 64];
	
	printf("Enter matrix A, separated by space: \n");
	getMatrix(a, &r, &c);
	printf("Enter matrix B, separated by space: \n");
	for(int i = 0; i < r * c; i++)
		scanf("%d", &b[i]);
	
	for(int i = 0; i < r * c; i++)
		sum[i] = a[i] + b[i];
	
	printf("\nMatrix A:\n");
	printMatrix(a, r, c);
	printf("Matrix B:\n");
	printMatrix(b, r, c);
	
	printf("\nSum:\n");
	printMatrix(sum, r, c);
	
	return 0;
}

void getMatrix(int a[], int* r, int* c){
	getArray(&a[0], c);
	*r = 0;
	for(int length = *c; length > 0 && length == *c; (*r)++)
		getArray(&a[*c * (*r + 1)], &length);
}

void getArray(int a[], int* length){
	char input[1024];
	gets(input);
	
	int inputLength = 0;
	while(input[inputLength] != '\0')
		inputLength++;
	while(input[inputLength - 1] == ' ')
		inputLength--;
	
	*length = 0;
	for(int i = 0; i < inputLength - 1; i++)
		if((i == 0) || (input[i] == ' ' && input[i + 1] != ' '))
			(*length)++;
	
	for(int i = 0; i < *length; i++)
		a[i] = 0;
	
	for(int i = 0, x = -1, isNegative = 0; i < inputLength; i++){
		if((i == 0) || (input[i] == ' ' && input[i + 1] != ' '))
			x++;
		
		if(input[i] == '-')
			isNegative = 1;
		
		if(input[i] == ' ' || input[i] == '-')
			continue;
		
		a[x] = a[x] * 10 + (isNegative?(-1):(1)) * (input[i] - '0');
	}
}

void printMatrix(int a[], int r, int c){
	for(int i = 0; i < r; i++)
		for(int j = 0; j < c; j++)
			printf("%-5d%c", a[i * c + j], (j == c - 1)?'\n':' ');
}