//Convert number to binary

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);
	
	int l = (n == 0)?1:(int) (log(n)/log(2)) + 1;
	char* s = malloc(l * sizeof(char) + 1);
	s[l] = 0;
	for(int i = 0; i < l; i++){
		s[l + ~i] = '0' + (n % 2);
		n /= 2;
	}
	
	printf("Number in Binary: %s", s);
	
	return 0;
}