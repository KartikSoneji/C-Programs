//Check if the string is a Palindrome or not

#include <stdio.h>

int isPalindrome(char*, int);

int main(){
	char s[1024];
	int len;
	
	printf("Enter a String: ");
	gets(s);
	printf("\n");
	
	len = 0;
	while(s[len++ + 1] != '\0') ;
	
	printf("String is %sa Palindrome", (isPalindrome(s, len))?"":"not ");
	
	return 0;
}

int isPalindrome(char* s, int l){
	for(int i = 0; i < l; i++)
		if(s[i] != s[l + ~i])
			return 0;
	
	return 1;
}