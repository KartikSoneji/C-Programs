//Print the following Pattern:
/*
a)	*		b)	1
	* *			2 3
	* * *		4 5 6
	* * * *		7 8 9 10
*/

#include <stdio.h>

int main(){
	int pattern, n;
	
	printf("Pattern (1) or Pattern (2): ");
	scanf("%d", &pattern);
	printf("Enter the number of lines: ");
	scanf("%d", &n);
	printf("\n");
	
	for(int i = 0, counter = 1; i < n; i++){
		for(int j = 0; j <= i; j++)
			if(pattern == 1)
				printf("* ");
			else
				printf("%d ", counter++);
		
		printf("\n");
	}
	
	printf("\n");
	
	return 0;
}