//Check if the number is Prime

#include <stdio.h>

int isPrime(int);

int main(){
	int a;
	
	printf("Enter a number: ");
	scanf("%d", &a);
	printf("\n");
	
	printf("Number is %sPrime", (isPrime(a))?"":"not ");
	
	return 0;
}

int isPrime(int n){
	if(n == 1 || n % 2 == 0)
		return (n == 2);
	
	for(int i = 3; i * i <= n; i++)
		if(n % i != 0)
			return 0;
	
	return 1;
}