//Find power of a number using recursion

#include <stdio.h>

float power(float, int);

int main(){
	float n;
	int p;
	
	printf("Enter (number) (power): ");
	scanf("%f%d", &n, &p);
	printf("\n");
	
	if(n - ((int) n) == 0)
		printf("%.0f ^ %d = %.0f", n, p, power(n, p));
	else
		printf("%.3f ^ %d = %.3f", n, p, power(n, p));
	
	return 0;
}

float power(float n, int p){
	if(p == 0)
		return 1;
	
	return n * power(n, p - 1);
}