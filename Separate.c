//Separate Even, Odd, Positive and Negative numbers in array

#include <stdio.h>
#include <stdlib.h>

int main(){
	int* a, length, inputLength;
	char input[1024];
	
	printf("Enter the array, separated by space: ");
	gets(input);
	printf("\n");
	
	inputLength = 0;
	while(input[inputLength] != '\0')
		inputLength++;
	while(input[inputLength - 1] == ' ')
		inputLength--;
	
	length = 0;
	for(int i = 0; i < inputLength - 1; i++)
		if((i == 0) || (input[i] == ' ' && input[i + 1] != ' '))
			length++;
	
	a = malloc(length * sizeof(int));
	for(int i = 0; i < length; i++)
		a[i] = 0;
	
	for(int i = 0, x = -1, isNegative = 0; i < inputLength; i++){
		if((i == 0) || (input[i] == ' ' && input[i + 1] != ' '))
			x++;
		
		if(input[i] == '-')
			isNegative = 1;
		
		if(input[i] == ' ' || input[i] == '-')
			continue;
		
		a[x] = a[x] * 10 + (isNegative?(-1):(1)) * (input[i] - '0');
	}
	
	printf("Odd Numbers:\t");
	for(int i = 0; i < length; i++)
		if(a[i] % 2 == 1)
			printf("%d ", a[i]);
	printf("\n");
	printf("Even Numbers:\t");
	for(int i = 0; i < length; i++)
		if(a[i] % 2 == 0)
			printf("%d ", a[i]);
	printf("\n\n");
	
	printf("Positive Numbers:\t");
	for(int i = 0; i < length; i++)
		if(a[i] >= 0)
			printf("%d ", a[i]);
	printf("\n");
	printf("Negative Numbers:\t");
	for(int i = 0; i < length; i++)
		if(a[i] < 0)
			printf("%d ", a[i]);
	printf("\n");
	
	return 0;
}