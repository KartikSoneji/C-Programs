//Find Sum of the Digits of a number

#include <stdio.h>

int main(){
	int a, sum;
	
	printf("Enter a number: ");
	scanf("%d", &a);
	printf("\n");
	
	if(a < 0)
		a = -a;
	
	sum = 0;
	while(a > 0){
		sum += a % 10;
		a /= 10;
	}
	
	printf("Sum of digits is %d", sum);
	
	return 0;
}