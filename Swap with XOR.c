//Swap two numbers without using a third variable

#include <stdio.h>

int main(){
	int a, b;
	
	printf("Enter two numbers: ");
	scanf("%d%d", &a, &b);
	
	a ^= b;
	b ^= a;
	a ^= b;
	
	printf("Swapped numbers are: %d\t%d", a, b);
	
	return 0;
}