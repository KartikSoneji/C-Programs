//Swap two numbers using functions

#include <stdio.h>

void swap(int*, int*);

int main(){
	int a, b;
	
	printf("Enter two numbers: ");
	scanf("%d%d", &a, &b);
	
	swap(&a, &b);
	
	printf("Swapped numbers are: %d\t%d", a, b);
	
	return 0;
}

void swap(int* a, int* b){
	*a ^= *b;
	*b ^= *a;
	*a ^= *b;
}